# Ansible Docker image
#
# Copyright (C) 2018, 2020-2025 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=3.11.11-alpine3.21
FROM python:${PYTHON_IMAGE_TAG}
LABEL org.opencontainers.image.authors='LSF operations team <ops@libre.space>'

ARG ANSIBLE_VERSION
ARG ANSIBLE_LINT_VERSION

ARG TARGETPLATFORM

# Disable Python compilation to bytecode
ENV PYTHONDONTWRITEBYTECODE=1

# Disable output buffering
ENV PYTHONUNBUFFERED=1

# Install system packages
RUN apk add --no-cache \
	git

# Add piwheels repository
RUN if [ "${TARGETPLATFORM:-linux/amd64}" = "linux/arm/v7" ]; then \
		echo "[global]" > /etc/pip.conf \
		&& echo "extra-index-url=https://www.piwheels.org/simple" >> /etc/pip.conf; \
	fi

# Install Python packages
RUN if [ "${TARGETPLATFORM:-linux/amd64}" = "linux/amd64" ]; then \
		ansible_lint_install=1; \
	fi \
	&& pip install --no-cache-dir --prefer-binary \
		ansible${ANSIBLE_VERSION:+==$ANSIBLE_VERSION} \
		${ansible_lint_install:+ansible-lint${ANSIBLE_LINT_VERSION:+==$ANSIBLE_LINT_VERSION}}
